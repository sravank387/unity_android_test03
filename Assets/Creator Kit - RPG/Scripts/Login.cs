﻿

using UnityEngine;


using Firebase.Auth;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    public static string PhoneVerificationId;
    public InputField Mobile;
    public Credential PhoneCredential;
    Firebase.Auth.FirebaseAuth auth;
    public InputField OTP;
    public void GetOTP()                                                                 //Function to send otp to the valid user mobile number
    {
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;    //Firebase auth instance creation to start authorization activity


        uint phoneAuthTimeoutMs = 10000;                                                 //
        PhoneAuthProvider provider = PhoneAuthProvider.GetInstance(auth);
        provider.VerifyPhoneNumber(Mobile.text, phoneAuthTimeoutMs, null,
            verificationCompleted: (credential) =>
            {

                Debug.Log("1");

                //OnVerifyOTP(credential);
            },
            verificationFailed: (error) =>
            {

                Debug.Log("Verification failed");


            },
            codeSent: (id, token) =>
            {
                PhoneVerificationId = id;
                Debug.Log(Mobile.text);
                Debug.Log("SMS Has been sent and the verification Id is  " + id);
            },
            codeAutoRetrievalTimeOut: (id) =>
            {

                Debug.Log("Code Retrieval Time out");

            });





    }

    public void OnclickVerify()
    {

        //Creating Phone Auth Credential

        PhoneAuthProvider provider = PhoneAuthProvider.GetInstance(auth);
        PhoneCredential = provider.GetCredential(PhoneVerificationId, OTP.text);
        SignUp();


    }

    public void SignUp()                                                              // Registers user through phone number and links with the mail
    {

     /*   auth.SignInWithCredentialAsync(PhoneCredential).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            FirebaseUser newUser = task.Result;
            

            Debug.Log(("User signed in successfully: {0} (ProviderID = {1})",
            newUser.PhoneNumber, newUser.ProviderId));

            // CreateUserDocument();


        });*/

       
        SceneManager.LoadScene("LoginSuccess");

    }

    public InputField Email, password;
    Firebase.Auth.FirebaseUser newUser;
    public void OnlyforEmailSignup()                         //use this function on Register button to sign in without phone authentication script..saves us from building apk to test
    {

        var auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.CreateUserWithEmailAndPasswordAsync(Email.text, password.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            newUser = task.Result;
           // SceneManager.LoadScene("LoginSuccess");
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            
            // Patient_UId = newUser.UserId;


        });

        SceneManager.LoadSceneAsync("sravan01");


    }



    public void Account2Function()
    {
        Debug.Log("line 1");

        Debug.Log("line 2");
    }




}
